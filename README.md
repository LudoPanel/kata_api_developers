# Readme

### Setup

* Run the application and request at `http://localhost:8080/api`

### Configuration

Open `application.yaml` to update :
* `port` : Port that Spring boot uses, default is `8080`

### Aviable Requests

| Method | Path | Body | Description
|:------ |:---- |:----:|:------------:|
| `POST`  | `/developers` | `{ firstName: string, lastName: string, languages : { id: long, name: string } }` | Create developer 
| `PUT`  | `/developers` | `{ id: long, firstName: string, lastName: string, languages : { id: long, name: string } }` | Update developer 
| `GET`  | `/developers` | `Request Param : idLanguage: long` | Get developers by id language 
| `POST`  | `/languages` | `{ name: string }` | Create language