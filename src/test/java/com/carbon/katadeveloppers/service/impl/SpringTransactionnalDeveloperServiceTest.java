package com.carbon.katadeveloppers.service.impl;

import com.carbon.katadeveloppers.entities.Developer;
import com.carbon.katadeveloppers.repository.DeveloperRepository;
import com.carbon.katadeveloppers.service.DeveloperService;
import com.carbon.katadeveloppers.service.dto.DeveloperDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class SpringTransactionnalDeveloperServiceTest {

    private static final String FIRST_NAME = "Panel";
    private static final String LAST_NAME = "Ludovic";
    private static final long FIRST_ID = 1L;

    @MockBean
    private DeveloperRepository developerRepository;

    private Developer developer;

    private DeveloperService developerService;

    @Before
    public void setUp() {
        developerService = new SpringTransactionnalDeveloperService(developerRepository);

        developer = new Developer();
        developer.setFirstName(FIRST_NAME);
        developer.setLastName(LAST_NAME);
    }

    @Test
    public void should_call_repository_to_save_a_new_developer() {
        Developer savedDeveloper = new Developer();
        savedDeveloper.setId(FIRST_ID);
        savedDeveloper.setFirstName(developer.getFirstName());
        savedDeveloper.setLastName(developer.getLastName());

        DeveloperDto developerDto = DeveloperDto.ofDeveloper(developer);

        when(developerRepository.save(developer)).thenReturn(developer);

        developerService.save(developerDto);

        ArgumentCaptor<Developer> argument = ArgumentCaptor.forClass(Developer.class);
        verify(developerRepository).save(argument.capture());
        assertEquals(argument.getValue(), developer);
    }

    @Test
    public void should_call_repository_to_get_developers_by_language_id() {
        when(developerRepository.findByLanguages_id(FIRST_ID)).thenReturn(Arrays.asList(developer));

        developerService.findByIdLanguage(FIRST_ID);

        ArgumentCaptor<Long> argument = ArgumentCaptor.forClass(Long.class);
        verify(developerRepository).findByLanguages_id(argument.capture());
        assertEquals(argument.getValue().longValue(), FIRST_ID);
    }

}