package com.carbon.katadeveloppers.service.impl;

import com.carbon.katadeveloppers.entities.Language;
import com.carbon.katadeveloppers.repository.LanguageRepository;
import com.carbon.katadeveloppers.service.LanguageService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class SpringTransactionnalLanguageServiceTest {

    private static final String JAVA = "Java";
    private static final long FIRST_ID = 1L;

    @MockBean
    private LanguageRepository languageRepository;

    private Language language;

    private LanguageService languageService;

    @Before
    public void setUp() {
        languageService = new SpringTransactionnalLanguageService(languageRepository);

        language = new Language();
        language.setName(JAVA);
    }

    @Test
    public void should_call_repository_to_save_a_new_language() {
        Language savedLanguage = new Language();
        savedLanguage.setId(FIRST_ID);
        savedLanguage.setName(language.getName());

        when(languageRepository.save(language)).thenReturn(language);

        languageService.add(language.getName());

        ArgumentCaptor<Language> argument = ArgumentCaptor.forClass(Language.class);
        verify(languageRepository).save(argument.capture());
        assertEquals(argument.getValue(), language);
    }

}