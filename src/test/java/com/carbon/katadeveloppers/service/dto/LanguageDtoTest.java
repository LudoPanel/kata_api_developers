package com.carbon.katadeveloppers.service.dto;

import com.carbon.katadeveloppers.entities.Language;
import org.junit.Test;

import static org.junit.Assert.*;

public class LanguageDtoTest {

    private static final String JAVA = "Java";

    @Test
    public void should_create_language_dto() {
        Language language = new Language();
        language.setName(JAVA);

        LanguageDto languageDto = LanguageDto.ofLanguage(language);

        assertEquals(languageDto.getName(), language.getName());
    }
}