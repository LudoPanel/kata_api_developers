package com.carbon.katadeveloppers.service.dto;

import com.carbon.katadeveloppers.entities.Developer;
import org.junit.Test;
import static org.junit.Assert.*;

public class DeveloperDtoTest {

    private static final String LAST_NAME = "Ludovic";
    private static final String FIRST_NAME = "Panel";

    @Test
    public void should_create_dto_by_developer() {
        Developer developer = new Developer();
        developer.setLastName(LAST_NAME);
        developer.setFirstName(FIRST_NAME);

        DeveloperDto developerDto = DeveloperDto.ofDeveloper(developer);

        assertEquals(developerDto.getLastName(), developer.getLastName());
        assertEquals(developerDto.getFirstName(), developer.getFirstName());
    }

}