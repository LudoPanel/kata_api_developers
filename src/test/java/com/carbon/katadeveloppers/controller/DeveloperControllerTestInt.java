package com.carbon.katadeveloppers.controller;

import com.carbon.katadeveloppers.entities.Developer;
import com.carbon.katadeveloppers.service.DeveloperService;
import com.carbon.katadeveloppers.service.dto.DeveloperDto;

import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(DeveloperController.class)
public class DeveloperControllerTestInt {

    private static final String URI_DEVELOPERS = "/api/developers";
    private static final String LUDOVIC = "Ludovic";
    private static final String FIRST_NAME = "Panel";
    private static final Long FIRST_ID = 1L;

    @Autowired
    private MockMvc mvc;

    @MockBean
    private DeveloperService developerService;

    private Developer initialDeveloper;

    @Before
    public void setUp() {
        initialDeveloper = new Developer();
        initialDeveloper.setLastName(LUDOVIC);
        initialDeveloper.setFirstName(FIRST_NAME);
    }

    @Test
    public void should_call_service_to_create_a_new_developer() throws Exception {
        Gson gson = new Gson();

        DeveloperDto developerDto = DeveloperDto.ofDeveloper(initialDeveloper);

        Developer savedDeveloper = new Developer();
        savedDeveloper.setLastName(LUDOVIC);
        savedDeveloper.setFirstName(FIRST_NAME);
        savedDeveloper.setId(FIRST_ID);

        DeveloperDto developerSaved = DeveloperDto.ofDeveloper(savedDeveloper);

        when(developerService.save(developerDto)).thenReturn(developerSaved);

        mvc.perform(
                post(URI_DEVELOPERS)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(gson.toJson(developerDto)))
                .andExpect(status().isCreated());

        ArgumentCaptor<DeveloperDto> argument = ArgumentCaptor.forClass(DeveloperDto.class);
        verify(developerService).save(argument.capture());
        assertEquals(argument.getValue(), developerDto);
    }

    @Test
    public void given_trying_to_create_developer_should_throw_exception_if_developer_already_exist() throws Exception {
        Gson gson = new Gson();

        initialDeveloper.setId(FIRST_ID);
        DeveloperDto developerDto = DeveloperDto.ofDeveloper(initialDeveloper);

        mvc.perform(
                post(URI_DEVELOPERS)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(gson.toJson(developerDto)))
                .andExpect(status().isBadRequest());

        verify(developerService, never()).save(developerDto);
    }

    @Test
    public void given_trying_to_update_developer_should_throw_exception_if_id_non_present() throws Exception {
        Gson gson = new Gson();

        DeveloperDto developerDto  = DeveloperDto.ofDeveloper(initialDeveloper);

        mvc.perform(
                put(URI_DEVELOPERS)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(gson.toJson(developerDto)))
                .andExpect(status().isBadRequest());

        verify(developerService, never()).save(developerDto);
    }

    @Test
    public void should_call_service_to_update_a_developer() throws Exception {
        Gson gson = new Gson();

        initialDeveloper.setId(FIRST_ID);
        Developer updatedDeveloper = initialDeveloper;
        updatedDeveloper.setLastName("Ludo");

        DeveloperDto developerUpdatedDto = DeveloperDto.ofDeveloper(updatedDeveloper);

        when(developerService.save(developerUpdatedDto)).thenReturn(developerUpdatedDto);

        mvc.perform(
                put(URI_DEVELOPERS)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(gson.toJson(developerUpdatedDto)))
                .andExpect(status().isOk());

        ArgumentCaptor<DeveloperDto> argument = ArgumentCaptor.forClass(DeveloperDto.class);
        verify(developerService).save(argument.capture());
        assertEquals(argument.getValue(), developerUpdatedDto);
    }

    @Test
    public void should_call_service_to_get_developers_by_id_language() throws Exception {
        initialDeveloper.setId(FIRST_ID);
        DeveloperDto developerDto = DeveloperDto.ofDeveloper(initialDeveloper);
        when(developerService.findByIdLanguage(FIRST_ID)).thenReturn(Arrays.asList(developerDto));

        MvcResult result = mvc.perform(get(URI_DEVELOPERS + "?idLanguage=1"))
                .andExpect(status().isOk())
                .andReturn();

        verify(developerService).findByIdLanguage(FIRST_ID);

    }

}