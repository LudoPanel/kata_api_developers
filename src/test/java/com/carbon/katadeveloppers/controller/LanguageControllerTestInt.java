package com.carbon.katadeveloppers.controller;

import com.carbon.katadeveloppers.entities.Language;
import com.carbon.katadeveloppers.service.LanguageService;
import com.carbon.katadeveloppers.service.dto.LanguageDto;
import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.Assert.*;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(LanguageController.class)
public class LanguageControllerTestInt {

    private static final String URI_LANGUAGES = "/api/languages";
    private static final String JAVA = "Java";
    private static final long FIRST_ID = 1L;

    @Autowired
    private MockMvc mvc;

    @MockBean
    private LanguageService languageService;

    private Language language;

    @Before
    public void setUp() {
        language = new Language();
        language.setName(JAVA);
    }

    @Test
    public void should_create_a_new_developer() throws Exception {
        Gson gson = new Gson();

        LanguageDto languageDto = LanguageDto.ofLanguage(language);

        Language savedLanguage = new Language();
        savedLanguage.setName(JAVA);
        savedLanguage.setId(FIRST_ID);

        LanguageDto languageSavedDto = LanguageDto.ofLanguage(savedLanguage);

        when(languageService.add(languageDto.getName())).thenReturn(languageSavedDto);

        mvc.perform(
                post(URI_LANGUAGES)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(gson.toJson(languageDto)))
                .andExpect(status().isCreated());

        ArgumentCaptor<String> argument = ArgumentCaptor.forClass(String.class);
        verify(languageService).add(argument.capture());
        assertEquals(argument.getValue(), languageDto.getName());
    }

    @Test
    public void given_trying_to_create_language_should_throw_exception_if_language_already_exist() throws Exception {
        Gson gson = new Gson();

        language.setId(FIRST_ID);
        LanguageDto languageDto = LanguageDto.ofLanguage(language);

        mvc.perform(
                post(URI_LANGUAGES)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(gson.toJson(languageDto)))
                .andExpect(status().isBadRequest());

        verify(languageService, never()).add(languageDto.getName());
    }
}