package com.carbon.katadeveloppers.service.impl;

import com.carbon.katadeveloppers.entities.Language;
import com.carbon.katadeveloppers.service.dto.DeveloperDto;
import com.carbon.katadeveloppers.repository.DeveloperRepository;
import com.carbon.katadeveloppers.entities.Developer;
import com.carbon.katadeveloppers.service.dto.LanguageDto;
import com.carbon.katadeveloppers.service.DeveloperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
public class SpringTransactionnalDeveloperService implements DeveloperService {

    private final DeveloperRepository developerRepository;

    @Autowired
    public SpringTransactionnalDeveloperService(DeveloperRepository developerRepository) {
        this.developerRepository = developerRepository;
    }

    @Override
    public DeveloperDto save(DeveloperDto developerDto) {
        Developer developer = new Developer();
        developer.setId(developerDto.getId());
        developer.setFirstName(developerDto.getFirstName());
        developer.setLastName(developerDto.getLastName());

        Set<Language> languages =  developerDto.getLanguages().stream().map(this::getLanguageByDto).collect(Collectors.toSet());

        developer.setLanguages(languages);

        developer = developerRepository.save(developer);

        return DeveloperDto.ofDeveloper(developer);
    }

    @Override
    public List<DeveloperDto> findByIdLanguage(Long idLanguage) {
        List<Developer> developers = developerRepository.findByLanguages_id(idLanguage);
        return developers.stream().map(DeveloperDto::ofDeveloper).collect(Collectors.toList());
    }

    private Language getLanguageByDto(LanguageDto languageDto) {
        Language language = new Language();
        language.setId(languageDto.getId());
        language.setName(languageDto.getName());
        return language;
    }
}
