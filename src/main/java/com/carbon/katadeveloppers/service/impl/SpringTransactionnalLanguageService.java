package com.carbon.katadeveloppers.service.impl;

import com.carbon.katadeveloppers.entities.Language;
import com.carbon.katadeveloppers.repository.LanguageRepository;
import com.carbon.katadeveloppers.service.LanguageService;
import com.carbon.katadeveloppers.service.dto.LanguageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class SpringTransactionnalLanguageService implements LanguageService {

    private final LanguageRepository languageRepository;

    @Autowired
    public SpringTransactionnalLanguageService(LanguageRepository languageRepository) {
        this.languageRepository = languageRepository;
    }

    @Override
    public LanguageDto add(final String name) {
        Language languageToAdd = new Language();
        languageToAdd.setName(name);

        Language savedLanguage = languageRepository.save(languageToAdd);

        return LanguageDto.ofLanguage(savedLanguage);
    }
}
