package com.carbon.katadeveloppers.service;

import com.carbon.katadeveloppers.service.dto.DeveloperDto;
import com.carbon.katadeveloppers.service.dto.LanguageDto;

import java.util.List;

public interface DeveloperService {
    DeveloperDto save(DeveloperDto developerDto);
    List<DeveloperDto> findByIdLanguage(Long id);
}
