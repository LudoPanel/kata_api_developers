package com.carbon.katadeveloppers.service;

import com.carbon.katadeveloppers.service.dto.LanguageDto;

public interface LanguageService {
    LanguageDto add(String name);
}
