package com.carbon.katadeveloppers.service.dto;

import com.carbon.katadeveloppers.entities.Developer;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

public class DeveloperDto implements Serializable {
    private Long id;
    private String firstName;
    private String lastName;
    private Set<LanguageDto> languages;

    public DeveloperDto() {
    }

    private DeveloperDto(Long id, final String firstName, final String lastName, Set<LanguageDto> languages) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.languages = languages;
    }

    public static DeveloperDto ofDeveloper(Developer developer) {
        Set<LanguageDto> languageDtos = developer.getLanguages().stream().map(LanguageDto::ofLanguage).collect(Collectors.toSet());

        return new DeveloperDto(developer.getId(), developer.getFirstName(), developer.getLastName(), languageDtos);
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Set<LanguageDto> getLanguages() {
        return languages == null ? Collections.EMPTY_SET : Collections.unmodifiableSet(languages);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeveloperDto that = (DeveloperDto) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(firstName, that.firstName) &&
                Objects.equals(lastName, that.lastName);
    }

}
