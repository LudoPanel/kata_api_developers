package com.carbon.katadeveloppers.service.dto;

import com.carbon.katadeveloppers.entities.Language;

import java.io.Serializable;
import java.util.Objects;

public class LanguageDto implements Serializable {

    private Long id;
    private String name;

    public LanguageDto() {
    }

    private LanguageDto(final Long id, final String name) {
        this.id = id;
        this.name = name;
    }

    public static LanguageDto ofLanguage(Language language) {
        return new LanguageDto(language.getId(), language.getName());
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LanguageDto that = (LanguageDto) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name);
    }
}

