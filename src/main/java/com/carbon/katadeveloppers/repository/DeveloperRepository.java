package com.carbon.katadeveloppers.repository;

import com.carbon.katadeveloppers.entities.Developer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DeveloperRepository extends JpaRepository<Developer, Long> {
    List<Developer> findByLanguages_id(Long id);
}
