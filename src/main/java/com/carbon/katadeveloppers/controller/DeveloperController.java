package com.carbon.katadeveloppers.controller;

import com.carbon.katadeveloppers.service.dto.DeveloperDto;
import com.carbon.katadeveloppers.service.DeveloperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping(DeveloperController.API_DEVELOPERS)
public class DeveloperController {

    public static final String API_DEVELOPERS = "/api/developers";
    public static final String SLASH_SEPARATOR = "/";

    private final DeveloperService developerService;

    @Autowired
    public DeveloperController(DeveloperService developerService) {
        this.developerService = developerService;
    }

    @PostMapping()
    public ResponseEntity<DeveloperDto> createDeveloper(@Valid @RequestBody DeveloperDto developerDto) throws URISyntaxException {
        if(developerDto.getId() != null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(developerDto);
        }

        DeveloperDto result = developerService.save(developerDto);

        URI redirectUri = new URI(API_DEVELOPERS + SLASH_SEPARATOR + result.getId());

        return ResponseEntity.created(redirectUri).body(result);
    }

    @PutMapping()
    public ResponseEntity<DeveloperDto> updateDeveloper(@Valid @RequestBody DeveloperDto developerDto) {
        if(developerDto.getId() == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(developerDto);
        }

        DeveloperDto result = developerService.save(developerDto);

        return ResponseEntity.ok().body(result);
    }

    @GetMapping(params = "idLanguage")
    public ResponseEntity<List<DeveloperDto>> getDevelopersByIdLanguage(@Valid @RequestParam Long idLanguage) {
        return ResponseEntity.status(HttpStatus.OK).body(developerService.findByIdLanguage(idLanguage));
    }
}
