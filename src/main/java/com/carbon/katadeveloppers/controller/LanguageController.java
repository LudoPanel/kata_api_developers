package com.carbon.katadeveloppers.controller;

import com.carbon.katadeveloppers.service.LanguageService;
import com.carbon.katadeveloppers.service.dto.LanguageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

@RestController
@RequestMapping(LanguageController.API_LANGUAGES)
public class LanguageController {

    public static final String API_LANGUAGES = "api/languages";
    public static final String SLASH_SEPARATOR = "/";

    private final LanguageService languageService;

    @Autowired
    public LanguageController(LanguageService languageService) {
        this.languageService = languageService;
    }

    @PostMapping()
    public ResponseEntity<LanguageDto> createLanguage(@Valid @RequestBody LanguageDto languageDto) throws URISyntaxException {
        if(languageDto.getId() != null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(languageDto);
        }

        LanguageDto result = languageService.add(languageDto.getName());

        URI redirectUri = new URI(API_LANGUAGES + SLASH_SEPARATOR + result.getId());

        return ResponseEntity.created(redirectUri).body(result);
    }
}
